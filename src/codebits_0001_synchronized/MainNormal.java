package codebits_0001_synchronized;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainNormal {
	private int count = 0;
	
	public void accessCount() {
		int currentCount = getCount();
		currentCount++;
		setCount(currentCount);
	}
	
	public static void main(String[] args) throws InterruptedException {
		MainNormal mn = new MainNormal();
		
		ExecutorService service = Executors.newFixedThreadPool(3);
		
		for(int i = 0; i < 1000; i++) {
			service.submit(mn::accessCount);
		}
		
		service.awaitTermination(1000, TimeUnit.MILLISECONDS);
		
		System.out.println(mn.getCount());
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
